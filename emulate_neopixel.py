from colr import color


class NeoPixel:
    brightness = 1

    def __init__(self, pin, length):
        self.n = length
        self.pixels = [(0, 0, 0) for i in range(self.n)]

    def write(self):
        print("\r", end="")
        for pixel in self.pixels:
            print(color(" ", back=pixel), end="")

    def __setitem__(self, key, value):
        for i in value:
            if type(i) != int:
                raise TypeError("Has to be an Integer")
        self.pixels[key] = value

    def __getitem__(self, key):
        return self.pixels[key]

    def fill(self, color):
        self.pixels = [color for i in range(self.n)]

