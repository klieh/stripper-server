import atexit

from sanic import Sanic
from sanic.response import text
from sanic.websocket import WebSocketProtocol
from sanic_cors import CORS

from leds import Led_stripe, Mode
from parse_json import parse_json
import os
import argparse

app = Sanic()
CORS(app)

app.led_stripe = Led_stripe()
app.led_stripe.loop()


@atexit.register
def atexit_func():
    app.led_stripe.timer.cancel()


from api import bp as api_bp
from website import bp as website_bp
from websocket_server import bp as websocket_server_bp


app.blueprint(api_bp)
app.blueprint(website_bp)
app.blueprint(websocket_server_bp)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Run the stripper server")
    parser.add_argument(
        "--port", "-p", type=int, default=8080, help="an integer for the accumulator"
    )
    parser.add_argument("--http", help="Enable http")
    args = parser.parse_args()

    if args.http or ("USE_HTTP" in os.environ and os.environ["USE_HTTP"]):
        ssl = None
    else:
        ssl = {"cert": "cert.pem", "key": "key.pem"}

    app.run(host="0.0.0.0", port=args.port, protocol=WebSocketProtocol, ssl=ssl)
