import asyncio
import json

from sanic import Blueprint

from parse_json import parse_json

bp = Blueprint("websocket_server")

USERS = set()


@bp.middleware("request")
async def add_key(request):
    if request.path == "/api":
        await notify_state(request.app.led_stripe)


def state_event(led_stripe):
    return json.dumps({"type": "state", **led_stripe.status})


def send_error(error):
    return json.dumps(error)


async def notify_state(led_stripe):
    if USERS:  # asyncio.wait doesn't accept an empty list
        message = state_event(led_stripe)
        await asyncio.wait([user.send(message) for user in USERS])


async def register(websocket):
    USERS.add(websocket)


async def unregister(websocket):
    USERS.remove(websocket)


@bp.websocket("/socket")
async def counter(request, websocket):
    led_stripe = request.app.led_stripe

    await register(websocket)
    try:
        await websocket.send(state_event(led_stripe))
        async for message in websocket:
            data = json.loads(message)
            print(data)
            error = parse_json(data, led_stripe)
            if error is not None:
                send_error(error)
            await notify_state(led_stripe)
    finally:
        await unregister(websocket)
