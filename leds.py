from enum import IntEnum
import threading
import os

if "EMULATE_NEOPIXEL" in os.environ and os.environ["EMULATE_NEOPIXEL"]:
    import emulate_neopixel as neopixel
    import emulate_board as board
else:
    import neopixel
    import board


class Mode(IntEnum):
    STATIC = 0
    SLIDE = 1


class Led_stripe:
    length = 60
    pin = board.D18
    np = neopixel.NeoPixel(pin, length)

    on = False
    mode = 0
    color = [255, 255, 255]
    gradient = -1
    position = 0
    speed = 100

    def __init__(self):
        self.loop()

    @property
    def status(self):
        return {
            "on": self.on,
            "mode": self.mode,
            "color": self.color,
            "gradient": self.gradient,
            "brightness": self.np.brightness * 255,
            "speed": self.speed,
            "status": "ok",
        }

    def _colorfull_gradient(self):
        r = 255
        g = 0
        b = 0
        value = 255 / self.np.n * 2
        r_add = -1
        g_add = +1
        b_add = 0

        for pos in range(self.np.n):
            if r < value and r_add == -1:
                r_add = 0
            elif r > 255 - value and r_add == +1:
                r_add = -1
                g_add = +1

            if g < value and g_add == -1:
                g_add = 0
            elif g > 255 - value and g_add == +1:
                g_add = -1
                b_add = +1

            if b < value and b_add == -1:
                b_add = 0
            elif b > 255 - value and b_add == +1:
                b_add = -1
                r_add = +1

            r += r_add * value
            g += g_add * value
            b += b_add * value

            if self.position + pos >= self.np.n:
                pos = pos - self.np.n

            self.np[int(self.position + pos)] = (int(r), int(g), int(b))

        self.np.write()

    def _light_gradient(self):
        color = [122, 255, 0]
        value = 255 / self.np.n * 2
        add = [True, False, True]

        for pos in range(self.np.n):
            for i in range(len(color)):
                if color[i] < value:
                    add[i] = True
                elif color[i] > 255 - value:
                    add[i] = False

                if add[i]:
                    color[i] += value
                else:
                    color[i] -= value

            if self.position + pos >= self.np.n:
                pos = pos - self.np.n

            self.np[int(self.position + pos)] = tuple(int(i) for i in color)

        self.np.write()

    def color_slide(self, gradient_f=_colorfull_gradient):
        add = (((self.speed - 100) / 255) + 1) ** 5
        self.position += add

        if self.position > self.np.n:
            self.position = 0

        gradient_f()

    def _color(self, color):
        self.np.fill(color)
        self.np.write()

    def _black(self):
        self._color((0, 0, 0))

    def loop(self):

        self.timer = threading.Timer(0.1, self.loop)
        self.timer.daemon = True
        self.timer.start()

        if self.on:
            if self.gradient == -1:
                self._color(self.color)
            else:
                if self.mode == Mode.STATIC:
                    if self.gradient == 0:
                        self._colorfull_gradient()
                    else:
                        self._light_gradient()
                else:
                    if self.gradient == 0:
                        self.color_slide(self._colorfull_gradient)
                    else:
                        self.color_slide(self._light_gradient)

        else:
            self._black()
