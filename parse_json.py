def between_0_and_255(number):
    return type(number) == int and number <= 255 and 0 < number


def parse_json(data, led_stripe):
    if data:
        if "on" in data:
            led_stripe.on = data["on"]

        if "brightness" in data:
            if not (between_0_and_255(data["brightness"])):
                return {"status": "error", "value": "brightness"}

            led_stripe.np.brightness = data["brightness"] / 255

        if "speed" in data:
            if not (between_0_and_255(data["speed"])):
                return {"status": "error", "value": "brightness"}

            led_stripe.speed = data["speed"]

        if "gradient" in data:
            if not type(data["gradient"]) == int:
                return {"status": "error", "value": "brightness"}
            led_stripe.gradient = data["gradient"]

        if "mode" in data:
            if not type(data["mode"]) == int:
                return {"status": "error", "value": "brightness"}
            led_stripe.mode = data["mode"]

        if "color" in data:
            if not (type(data["color"]) == list and len(data["color"]) == 3):
                return {"status": "error", "value": "brightness"}

            color = data["color"]

            if not (
                color[0] >= 0
                and color[0] <= 255
                and color[1] >= 0
                and color[1] <= 255
                and color[2] >= 0
                and color[2] <= 255
            ):
                print("Error")
                return {"status": "error"}

            led_stripe.color = color
