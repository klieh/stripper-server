from sanic import Blueprint
from sanic.response import text

bp = Blueprint("website")


@bp.route("/")
async def index(request):
    return text("Hi")
