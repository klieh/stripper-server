from sanic.response import json
from parse_json import parse_json
from sanic import Blueprint


bp = Blueprint("api")


@bp.route("/api/", methods=["POST", "GET", "OPTIONS"])
async def set(request):
    led_stripe = request.app.led_stripe
    error = parse_json(request.json, led_stripe)
    if error:
        return json(error)
    return json(led_stripe.status)
